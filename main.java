import java.util.Scanner;
import TTT.*;

public class main {
	public static Scanner in=new Scanner(System.in);
	public static void main(String [] args) {

		bot2 test=new bot2();
		board br=new board();
		botAlgorithm ba=new botAlgorithm();
		int [] playerDot=new int[2];

		while(!ba.row3(br.copyCon())&&br.mapValue(0).size()>0) {

			int [] botDot;

			System.out.println("board:");
			br.printBr();

			playerDot[0]=in.nextInt();
			playerDot[1]=in.nextInt();
			while(!(playerDot[0]>-1&&playerDot[0]<3&&playerDot[1]>-1&&playerDot[1]<3)||br.getPoint(playerDot[0], playerDot[1])!=0) {
				System.out.println("you can't play there, try another location");
				playerDot[0]=in.nextInt();
				playerDot[1]=in.nextInt();
			}
			br.playAt(playerDot[0], playerDot[1]);

			botDot=test.findNextPoint(br);
			if(botDot!=null)
				br.playAt(botDot[0], botDot[1]);
		}

		if(!ba.row3(br.copyCon())) {
			System.out.println("tie");
		}

		br.printBr();
	}
}
