# magic tic-tac-toe
## please don't kill me
this is an old project that I created before knowing how to write a good code (thanks school for not failing me) and when my English was more broken then now.<br />
this code is very bad, please do not learn from it.<br />
use/read this code **at your own risk**, there are probably some vulnerabilities in it.<br />
also, I plan to create a better version for this code in the future, so stay tuned.

## what is it?
its a code for an algorithm that can't lose in tic-tac-toe

## How the algorithm works (safe to read)
to understand the board and understand if something will lead to win and to understand what a win is, the algorithm have this map:<br />

6|1|8 <br />
7|5|3 <br />
2|9|4 <br />

this map is a 3x3 map that alows the algorithm to "understand" the game, but how? <br />
each cell have a different value, And when you add up the value with the values of two other cells, if their combined value is 15 then it is a winning row of cells, which means that one of the players won. take for example the cells 7-5-3, if you add up their values, it will be 15 and will be a win, but if you take the cells 1-5-3, it will be 9 and not an acceptable line. <br />
this way, the algorithm know what to prevent using some functions.
