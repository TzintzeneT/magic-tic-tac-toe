package TTT;
public class bot2 {

	//find the next point the bot should play
	public int [] findNextPoint(board br){

		botAlgorithm ba=new botAlgorithm();
		int [] nextMove=null;

		//check if bot can win
		nextMove=ba.CanCompleteSequenceOf3(br.getPlayer2BoardValue(), br);
		if(nextMove!=null)
			return nextMove;

		//check if player can win
		nextMove=ba.CanCompleteSequenceOf3(br.getPlayer1BoardValue(), br);
		if(nextMove!=null)
			return nextMove;

		//check if player can double chess and if so block it
		nextMove=ba.doubleChessV2(br.getPlayer1BoardValue(), br.getPlayer2BoardValue(), br);
		if(nextMove!=null)
			return nextMove;

		//select a generic point in the best location
		nextMove=ba.genericPoint(br);
		if(nextMove!=null)
			return nextMove;

		return nextMove;
	}

}
