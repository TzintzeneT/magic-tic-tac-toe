package TTT;
import java.util.ArrayList;
import java.util.List;

public class board {

	private int [][] board=new int[3][3];
	private int player1BoardValue=1;
	private int player2BoardValue=2;
	private int turn = 0;

	public board(){}

	//create a board with special values for the players
	public board(int player1BoardValue, int player2BoardValue){
		this.player1BoardValue = player1BoardValue;
		this.player2BoardValue = player2BoardValue;
	}

	public List<Integer> mapValue(int value) {

		List<Integer> pointslist=new ArrayList<Integer>();

		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				if(this.getPoint(i, j)==value)
					pointslist.add(i+j*10);
		System.out.println("points with the value "+value+" (rtl): "+pointslist);
		System.out.println();
		return pointslist;
	}

	public boolean canPlayAt(int a, int b) {
		if(this.board[a][b]==0)
			return true;
		return false;
	}

	public board copyCon() {
		board br=new board();
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				br.board[i][j]=this.board[i][j];
		return br;
	}

	public void printBr() {
		System.out.println("-------------");
		for(int i=0;i<3;i++) {
			System.out.print("| ");
			for(int j=0;j<3;j++) {
				System.out.print(this.board[i][j]);
				System.out.print(" | ");
			}
		System.out.println();
		System.out.println("-------------");
		}
	}

	public int getPoint(int i, int j) {
		return this.board[i][j];
	}

	//play a move on rhe board
  public void playAt(int x, int y){
    if(this.board[x][y] == 0){
      if (turn%2 == 0)
        this.board[x][y] = player1BoardValue;
      else
        this.board[x][y] = player2BoardValue;
      turn++;
    }
  }

	//get the turn number of the game
  public int getTurn(){
    return this.turn;
  }

	public void setPoint(int x, int y, int val){
		this.board[x][y] = val;
	}

	public int getPlayer1BoardValue() {
		return player1BoardValue;
	}

	public int getPlayer2BoardValue() {
		return player2BoardValue;
	}

}
