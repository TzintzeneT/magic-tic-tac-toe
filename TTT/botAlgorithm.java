package TTT;
import java.util.ArrayList;
import java.util.List;

public class botAlgorithm {

	private final int [][] boardValues = {
		{6,1,8},
		{7,5,3},
		{2,9,4}
	};

	private final int [][] valuessMap = {
		{0,0}, //ignore
		{0,1},{2,0},{1,2},
		{2,2},{1,1},{0,0},
		{1,0},{0,2},{2,1}
	};

	private final int [][] bestPoints= {
		{1,0,1},
		{0,2,0},
		{1,0,1}
	};

	//operations
	//check where player need to play to win
	//used
	public int [][] CanCompleteV2(int playerBoardVal, board br) {

		List<Integer> pointslist=this.mapValue(br, playerBoardVal);
		int [][] winPoints=new int[3][3];
		int a, b, c, d, e;

		//get point 1 for check
		for(int i=0;i<pointslist.size();i++) {
			a=pointslist.get(i)%10;
			b=pointslist.get(i)/10;

			//get point 2 for check
			for(int j=i+1;j<pointslist.size();j++) {
				c=pointslist.get(j)%10;
				d=pointslist.get(j)/10;

				//clac where the 3 point for win
				e=15-this.boardValues[a][b]-this.boardValues[c][d];

				//can player win? need block?
				if(e>0&&e<10&&e!=this.boardValues[a][b]&&e!=this.boardValues[c][d]&&br.getPoint(this.valuessMap[e][0], this.valuessMap[e][1])==0) {
					winPoints[this.valuessMap[e][0]][this.valuessMap[e][1]]=1;
				}
			}
		}
		return winPoints;
	}

	//Find out if there are 2 points that can be completed to 3 points
	//used
	public int [] CanCompleteSequenceOf3(int boardValueFL, board br) {

		List<Integer> pointslist=this.mapValue(br, boardValueFL);
		int [] point=new int[2];
		int a, b, c, d, e;

		//get point 1 for check
		for(int i=0;i<pointslist.size();i++) {
			a=pointslist.get(i)%10;
			b=pointslist.get(i)/10;

			//get point 2 for check
			for(int j=i+1;j<pointslist.size();j++) {
				c=pointslist.get(j)%10;
				d=pointslist.get(j)/10;

				//clac where the 3 point for win
				e=15-this.boardValues[a][b]-this.boardValues[c][d];

				//print all the points
				System.out.println("the 2 points:");
				System.out.println("1. "+this.boardValues[a][b]);
				System.out.println("2. "+this.boardValues[c][d]);
				System.out.println("the value that point 3 shoud have: "+e);

				//can player win? need block?
				if(e>0&&e<10&&e!=this.boardValues[a][b]&&e!=this.boardValues[c][d]&&br.getPoint(this.valuessMap[e][0], this.valuessMap[e][1])==0) {
					//find the point and print its value (e) if the point exist
					System.out.println("the algorithm have found an empty point with the value of "+this.boardValues[this.valuessMap[e][0]][this.valuessMap[e][1]]);
					//set the next move to block
					point[0]=this.valuessMap[e][0];
					point[1]=this.valuessMap[e][1];
					//print where to block
					System.out.println("the third point of ["+a+", "+b+"], ["+c+", "+d+"] is: ["+point[0]+", "+point[1]+"]");
					System.out.println();
					return point;
				}
			}
		}
		System.out.println("there isn't any matching empty point on the board");
		System.out.println();
		return null;
	}

	//Map all points on a board that have a certain value (all points that are empty, player 1 points or player 2 points)
	//used
	public List<Integer> mapValue(board br, int value) {

		return br.mapValue(value);
	}

	//count how much points player can play that will win
	//used
	public int doubleChessCount(int num, board br) {

		int [][] check=this.CanCompleteV2(num, br);
		int count=0;
		for(int i=0;i<3;i++)
			for(int j=0;j<3;j++)
				if(check[i][j]==1)
					count++;

		return count;
	}

	//check if a player can play at a specific point the next turn and win turn later with 2 Threats on different points on the board
	//used
	public int [] doubleChessV2(int chesserBoardVal, int antiChesserBoardVal, board br) {

		board brt=br.copyCon(), brtt=brt.copyCon();
		List<Integer> emptyPointsList=this.mapValue(br, 0);
		int [][] count=this.CanCompleteV2(chesserBoardVal, brt);
		int [] endPoint=new int[2];

		for(int point: emptyPointsList) {

			brt=br.copyCon();
			brt.setPoint((point%10), (point/10), chesserBoardVal);

			count=this.CanCompleteV2(chesserBoardVal, brt);
			brtt=brt.copyCon();

			for(int i=0;i<3;i++) {
				for(int j=0;j<3;j++) {
					if(count[i][j]==1) {
						brtt.setPoint(i, j, antiChesserBoardVal);
						if(this.doubleChessCount(chesserBoardVal, brt)>1&&this.CanCompleteSequenceOf3(antiChesserBoardVal, brtt)!=null) {
							System.out.println("counter: "+this.doubleChessCount(chesserBoardVal, brt));
							endPoint[0]=i;
							endPoint[1]=j;
							return endPoint;
						}
						brtt=brt.copyCon();
					}
				}
			}
		}
		return null;
	}

	//find the best generic point
	//used
	public int [] genericPoint(board br) {

		List<Integer> freePoints=this.mapValue(br, 0);
		int [] bestGenericPoint=new int[2];
		int max=-1;

		for(int point: freePoints) {
			if(this.bestPoints[point%10][point/10]>max) {
				bestGenericPoint[0]=(point%10);
				bestGenericPoint[1]=(point/10);
				max=this.bestPoints[point%10][point/10];
			}
		}

		if(max>-1)
			return bestGenericPoint;
		return null;
	}

	//check if one of the players completed a row of 3
	//used
	public boolean row3(board br) {

		List<Integer> pointslist=br.mapValue(br.getPlayer1BoardValue());
		int a, b, c, d, e;

		//run on the 2 player
		for(int h=0;h<2;h++) {

			//get point 1 for check
			for(int i=0;i<pointslist.size();i++) {
				a=pointslist.get(i)%10;
				b=pointslist.get(i)/10;

				//get point 2 for check
				for(int j=(i+1);j<pointslist.size();j++) {
					c=pointslist.get(j)%10;
					d=pointslist.get(j)/10;

					//clac where the 3 point for 3 in the row
					e=15-this.boardValues[a][b]-this.boardValues[c][d];

					//print all the points
					System.out.println("the 2 points:");
					System.out.println("1. "+this.boardValues[a][b]);
					System.out.println("2. "+this.boardValues[c][d]);
					System.out.println("the value that point 3 shoud have: "+e);

					//is the 3rd point owned by the player?
					if(e>0&&e<10&&e!=this.boardValues[a][b]&&e!=this.boardValues[c][d]&&br.getPoint(this.valuessMap[e][0], this.valuessMap[e][1])==br.getPoint((pointslist.get(0)%10), (pointslist.get(0)/10))) {
						System.out.println("the game was won by: "+br.getPoint((pointslist.get(0)%10), (pointslist.get(0)/10)));
						return true;
					}
				}
			}

			//move to the 2nd player
			pointslist=br.mapValue(br.getPlayer2BoardValue());
		}
		System.out.println("no one won this turn");
		System.out.println();

		return false;
	}

	public static void main(String [] args) {
		board br=new board();
		botAlgorithm ba=new botAlgorithm();
		br.setPoint(0, 0, br.getPlayer1BoardValue());
		br.setPoint(1, 1, br.getPlayer2BoardValue());
		br.setPoint(2, 2, br.getPlayer1BoardValue());
		//br.setPoint(2, 2, br.getPlayer1BoardValue());
		br.printBr();

		int [] calc3=ba.doubleChessV2(br.getPlayer1BoardValue(), br.getPlayer2BoardValue(), br);

		if(calc3!=null)
			System.out.println(calc3[0]+" "+calc3[1]);
	}
}
